import Vue from 'vue'
import Router from 'vue-router'
import Login from './views/Login.vue'
import Search from './views/Search.vue'
import Result from './views/Result.vue'
import Wishlist from './views/Wishlist.vue'
import Settings from './views/Settings.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'login',
      component: Login
    },
    {
      path: '/search',
      name: 'search',
      component: Search
    },
    {
      path: '/result',
      name: 'result',
      component: Result,
      props: true
    },
    {
      path: '/wishlist',
      name: 'wishlist',
      component: Wishlist
    },
    {
      path: '/settings',
      name: 'settings',
      component: Settings
    },
  ]
})
