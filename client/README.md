Multimedia Datenbanken (Aufbaukurs)

Features:
* Fuzzy Operator für Wörter die ähnlich klingen (Battlevield)
* Suchkombinationen:
    ** Name + Konsole
    ** nur Name / Konsole / Publisher / USK
* Wunschliste kann befüllt und geleert werden für jeden Nutzer
* Detailansicht zu spielen
* Suche mit selbstgemachten Bild
    ** gut: CoD (25), Batman hell (25)
    ** schlecht: Fifa (35), Batman dunkel (35)
    ** nichts: Fifa (25), Batman dunkel (25)
* Login / Registrierung / Abmeldung
