const express = require('express');
const multer = require('multer');
let oracledb = require('oracledb');

const app = express.Router();
app.use(express.static('public'));

const config = {
    user: 'mmdb09',
    password: 'mmdb09',
    connectString: 'ora10glv.imn.htwk-leipzig.de:1521/ora10glv'
};

var storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'public/images/')
    },
    filename: (req, file, cb) => {
        cb(null, file.fieldname + '-' + Date.now())
    }
});

var upload = multer({storage: storage});

app.all('/', function (req, res, next) {
    console.log({method: req.method, url: req.url});
    next();
});

// POST method route
app.get('/', async function (req, res) {
    console.log('GET dashboard');
    res.render('dashboard');
});

//TODO turoial: https://www.npmjs.com/package/multer#readme
app.post('/', upload.single('imageupload'), async function (req, res) {
    console.log(req.file);
    res.send("File upload sucessfully.");
    let conn;
    try {
        oracledb.autoCommit = true;

        conn = await oracledb.getConnection(config, async function (err, conn) {
                if (err) {
                    throw err;
                }
                const clear = 'delete from myphoto';
                const clearResult = await conn.execute(clear, async function (err, result) {
                    if (err) {
                        console.error(err.message);
                        return;
                    }
                    const sql =
                        "DECLARE\n" +
                        "imageObj ORDSYS.ORDImage;\n" +
                        "ctx RAW(4000) := NULL;\n" +
                        "fileName VARCHAR(20);\n" +
                        "BEGIN\n" +
                        "INSERT INTO myphoto (id, image, image_sig) VALUES ( 1, ORDSYS.ORDImage.init(), ORDSYS.ORDImageSignature.init() );\n" +
                        "SELECT image INTO imageObj FROM myphoto for UPDATE;\n" +
                        "imageObj.setSource('file', 'IMGDIR25' ,'picture001.jpg');\n" +
                        "imageObj.import(ctx);\n" +
                        "UPDATE myphoto SET image = imageObj;\n" +
                        "COMMIT;\n" +
                        "END;";

                    binds = [];
                    // For a complete list of options see the documentation.
                    options = {
                        autoCommit: true,
                    };

                    var result = conn.execute(sql, [], {autoCommit: true});
                    console.log(result);
                    console.log('geklappt');
                    //
                    // // //todo signatur funktioniert nur in einer Schleife, wieso?
                    // // // 30625. 00000 -  "method dispatch on NULL SELF argument is disallowed":
                    const nextsql =
                        "DECLARE\n" +
                        "    imageObj ORDSYS.ORDImage;\n" +
                        "    image_sigObj ORDSYS.ORDImageSignature;\n" +
                        "BEGIN\n" +
                        "      SELECT image, image_sig INTO imageObj, image_sigObj \n" +
                        "         FROM myphoto WHERE id=1 FOR UPDATE;\n" +
                        "      -- generate a signature\n" +
                        "      image_sigObj.generateSignature(imageObj);\n" +
                        "      UPDATE myphoto SET image_sig = image_sigObj WHERE id =1;\n" +
                        "    COMMIT;\n" +
                        "END;";

                    var result = conn.execute(nextsql, [], {autoCommit: true});
                    console.log('hat auch geklappt');
                    console.log(result.rows);

                    const selectSQL = 'SELECT id, p.image.getHeight() as height, p.image.getFileFormat() as format FROM myphoto p WHERE id=1';
                    const selectSQLResult = await conn.execute(selectSQL, []);
                    console.log(selectSQLResult.rows);
                });
            }
        );
// res.render('login.pug', {error: 'Speichern in DB erfolgreich'});
    } catch
        (err) {
        console.log('Ouch!', err);
        // res.render('login.pug', {error: 'Speichern in DB fehlgeschlagen'});
    } finally {
        if (conn) { // conn assignment worked, need to close
            conn.close();
        }
    }
})
;

module.exports = app;
