let oracledb = require('oracledb');
const app = require('./index');
const multer = require("multer");
const sftpStorage = require('multer-sftp');
const CONFIG = require('./config.json');
const fs = require('fs');
let Client = require('ssh2-sftp-client');
const path = require('path');

oracledb.fetchAsBuffer = [oracledb.BLOB];

const config = {
    user: 'mmdb09',
    password: 'mmdb09',
    connectString: 'ora10glv.imn.htwk-leipzig.de:1521/ora10glv'
};

const storage = sftpStorage({
    sftp: {
        host: 'kain.imn.htwk-leipzig.de',
        // port: '8080',
        username: CONFIG.username,
        password: CONFIG.password,
        port: 22,
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname)
    },
    destination: function (req, file, cb) {
        cb(null, '')
    },
});

const upload = multer({storage: storage});

app.all('/', function (req, res, next) {
    console.log({method: req.method, url: req.url});
    next();
});

app.post('/search/meta', async (req, res) => {
    console.log(req.body);
    const gameName = req.body.gameName.replace(/\s/g, "");
    const consoleType = req.body.consoleType;
    const publisher = req.body.publisher;
    const usk = req.body.usk;

    let query = '';

    if (gameName !== '' && consoleType !== '') {
        query = "select image.imageblob\n" +
            "from \n" +
            "    (select gameImages.imageid\n" +
            "    from \n" +
            "        (SELECT imageid \n" +
            "        FROM game \n" +
            "        INNER JOIN gamehasimage ON game.gameid = gamehasimage.gameid \n" +
            "        WHERE CONTAINS ( name, 'fuzzy(" + gameName + ")' ) >0)gameImages" +
            "        inner join (select imageid\n" +
            "                    from platform\n" +
            "                    inner join belongstoimage on platform.platformid = belongstoimage.platformid\n" +
            "                    where platform.platformname = '" + consoleType + "')platformImages\n" +
            "                    on gameImages.imageid = platformImages.imageid)p\n" +
            "inner join image on p.imageid = image.imageid"
    } else if (gameName !== '') {
        query = `
        select b.imageblob, b.name, b.usk, b.platformname, b.price, publisher.pubname, b.imageid
        from
            (select a.imageblob, a.name, a.usk, a.platformname, a.price, a.imageid, publishedby.pubid
            from
                (select data.imageblob, data.name, data.gameid, data.usk, platform.platformname, data.price, data.imageid
                from
                    (select image.imageblob, image.imageid, platformdata.gameid, platformdata.name, platformdata.usk, platformdata.platformid, platformdata.price
                    from 
                        (select platforminfo.imageid, platforminfo.gameid, platforminfo.name, platforminfo.usk, platforminfo.platformid, releasedon.price
                        from
                            (select gameinfo.imageid, gameinfo.gameid, gameinfo.name, gameinfo.usk, belongstoimage.platformid
                            from
                                (select imageids.imageid, imageids.gameid, game.name, game.usk
                                from
                                    (SELECT imageid, game.gameid
                                    FROM game
                                    INNER JOIN gamehasimage ON game.gameid = gamehasimage.gameid
                                    WHERE CONTAINS ( name, 'fuzzy(${gameName})' ) >0)imageids
                                inner join game on imageids.gameid = game.gameid)gameinfo    
                            inner join belongstoimage on gameinfo.imageid = belongstoimage.imageid)platforminfo  
                        inner join releasedon on platforminfo.gameid = releasedon.gameid AND platforminfo.platformid = releasedon.platformid)platformdata    
                    inner join image on platformdata.imageid = image.imageid) data
                inner join platform on data.platformid = platform.platformid ) a
            inner join publishedby on a.gameid = publishedby.gameid) b
        inner join publisher on b.pubid = publisher.pubid
        `;
    } else if (consoleType !== '') {
        query = `
        select e.imageblob, e.name, e.usk, e.platformname, e.price, publisher.pubname, e.imageid
        from
            (select d.imageblob, d.imageid, d.platformname, d.platformid, d.gameid, d.name, d.usk, d.price, publishedby.pubid
            from
                (select c.imageblob, c.imageid, c.platformname, c.platformid, c.gameid, c.name, c.usk, releasedon.price
                from
                    (select b.imageblob, b.imageid, b.platformname, b.platformid, b.gameid, game.name, game.usk
                    from
                        (select a.imageblob, a.imageid, a.platformname, a.platformid, gamehasimage.gameid
                        from
                            (select image.imageblob, imageids.imageid, imageids.platformname, imageids.platformid
                            from
                                (select imageid, platform.platformid, platform.platformname
                                from platform
                                inner join belongstoimage on platform.platformid = belongstoimage.platformid
                                where platform.platformname = '${consoleType}') imageids
                            inner join image on imageids.imageid = image.imageid) a
                        inner join gamehasimage on a.imageid = gamehasimage.imageid) b
                    inner join game on b.gameid = game.gameid) c
                inner join releasedon on c.gameid = releasedon.gameid and c.platformid = releasedon.platformid) d
            inner join publishedby on d.gameid = publishedby.gameid) e
        inner join publisher on e.pubid = publisher.pubid
        `;
    } else if (publisher !== '') {
        query = `
        select  data.imageblob, data.name, data.usk, platform.platformname, data.price, data.pubname, data.imageid
        from
            (select image.imageblob, image.imageid, platformdata.gameid, platformdata.pubname, platformdata.name, platformdata.usk, platformdata.platformid, platformdata.price
            from 
                (select platforminfo.imageid, platforminfo.gameid, platforminfo.pubname, platforminfo.name, platforminfo.usk, platforminfo.platformid, releasedon.price
                from
                    (select gameinfo.imageid, gameinfo.gameid, gameinfo.pubname, gameinfo.name, gameinfo.usk, belongstoimage.platformid
                    from
                        (select imageids.imageid, imageids.gameid, imageids.pubname, game.name, game.usk
                        from
                            (select imageid, gameids.gameid, gameids.pubname
                            from (select gameid, publisher.pubname
                                from publisher
                                inner join publishedby on publisher.pubid = publishedby.pubid
                                where publisher.pubname = '${publisher}')gameids
                            inner join gamehasimage on gameids.gameid = gamehasimage.gameid)imageids
                        inner join game on imageids.gameid = game.gameid)gameinfo    
                    inner join belongstoimage on gameinfo.imageid = belongstoimage.imageid)platforminfo  
                inner join releasedon on platforminfo.gameid = releasedon.gameid AND platforminfo.platformid = releasedon.platformid)platformdata    
            inner join image on platformdata.imageid = image.imageid) data
        inner join platform on data.platformid = platform.platformid
        `;
    } else if (usk !== '') {
        query = `
       SELECT
    n.imageblob,
    l.name AS name,
    l.usk,
    p.platformname,
    o.price,
    s.pubname,
    n.imageid
FROM
    gamehasimage     m,
    game             l,
    image            n,
    releasedon       o,
    platform         p,
    belongstoimage   q,
    publishedby      r,
    publisher        s
WHERE
    l.gameid = m.gameid
    AND n.imageid = m.imageid
    AND l.gameid = o.gameid
    AND p.platformid = o.platformid
    AND n.imageid = q.imageid And P.platformid = q.platformid
    AND l.gameid = r.gameid
    AND s.pubid = r.pubid
    AND l.usk = '${usk}'
        `
    }
    let conn;
    try {
        oracledb.autoCommit = true;
        await oracledb.getConnection(config).then(function (conn) {
            return conn.execute(query)
                .then(function (result) {
                    const directory = 'public/images/';
                    fs.readdir(directory, (err, files) => {
                        if (err) throw err;

                        for (const file of files) {
                            fs.unlink(path.join(directory, file), err => {
                                if (err) throw err;
                            });
                        }
                    });
                    res.json(result);
                    return conn.close();
                })
                .catch(function (err) {
                    console.log(err);
                    return conn.close();
                })
        })
    } catch
        (err) {
        console.log('Ouch!', err);
        res.json('nix');
    } finally {
        if (conn) { // conn assignment worked, need to close
            conn.close();
        }
    }
});

app.post('/search/add', async (req, res) => {
    console.log(req.body)
    oracledb.autoCommit = true;
    await oracledb.getConnection(config, async function (err, conn) {
        const setWishlistIncludesImages = `insert into wishlistincludesimages (wishlistid, imageid) values ('${req.body.user}', '${req.body.imageid}')`;
        conn.execute(setWishlistIncludesImages, []);
    });
});

app.post('/search/delete', async (req, res) => {
    oracledb.autoCommit = true;
    console.log(req.body)
    await oracledb.getConnection(config, async function (err, conn) {
        const setWishlistIncludesImages = `delete from wishlistincludesimages where imageid = '${req.body.imageid}'`;
        conn.execute(setWishlistIncludesImages, []);
    });
});

app.post('/load/wishlist', async (req, res) => {
    oracledb.autoCommit = true;
    console.log(req.body);
    await oracledb.getConnection(config, async function (err, conn) {
        const setWishlistIncludesImages = `
        SELECT
            t.imageblob,
            w.name,
            w.usk,
            v.platformname,
            aa.price,
            z.pubname,
            t.imageid
        FROM
            belongstoimage           u,
            image                    t,
            platform                 v,
            gamehasimage             x, 
            GAME                     w,
            publishedby              y,
            publisher                z,
            releasedon               aa,
            wishlistincludesimages   bb
        WHERE
            t.imageid = u.imageid
            AND v.platformid = u.platformid
            AND w.gameid = x.gameid
            AND t.imageid = x.imageid
            AND w.gameid = y.gameid
            AND z.pubid = y.pubid And w.gameid = aa.gameid
            AND v.platformid = aa.platformid
            AND t.imageid = bb.imageid
            AND bb.wishlistid = '${req.body.user}'`;
        const result = await conn.execute(setWishlistIncludesImages, []);
        console.log(result)
        res.send(result);
    });
});

app.post('/search/upload', upload.single('file'), async (req, res) => {
        let conn;
        let sftp = new Client();
        try {
            oracledb.autoCommit = true;
            conn = await oracledb.getConnection(config, async function (err, conn) {
                    if (err) {
                        throw err;
                    }
                    const clear = 'delete from image where imageid = 999';
                    await conn.execute(clear, async function (err, result) {
                        if (err) {
                            console.error(err.message);
                        } else {
                            console.log("Deleted imageid = 999")
                        }

                        const insertORDIMAGESQL =
                            "DECLARE\n" +
                            "imageObj ORDSYS.ORDImage;\n" +
                            "ctx RAW(4000) := NULL;\n" +
                            "fileName VARCHAR(20);\n" +
                            "BEGIN\n" +
                            "INSERT INTO image (imageId, image, image_sig) VALUES ( 999, ORDSYS.ORDImage.init(), ORDSYS.ORDImageSignature.init() );\n" +
                            "SELECT image INTO imageObj FROM image where imageid = 999 for UPDATE;\n" +
                            "imageObj.setSource('file', 'IMGDIR25' , '" + req.file.originalname + "');\n" +
                            "imageObj.import(ctx);\n" +
                            "UPDATE image SET image = imageObj where imageid = 999;\n" +
                            "COMMIT;\n" +
                            "END;";
                        conn.execute(insertORDIMAGESQL, [], {autoCommit: true}, function (err, result) {
                            if (err) {
                                console.error(err.message);
                            } else {
                                console.log("inserted image as ordsys")
                            }
                        });

                        const updateORDIMAGESQL =
                            "DECLARE\n" +
                            "    imageObj ORDSYS.ORDImage;\n" +
                            "    image_sigObj ORDSYS.ORDImageSignature;\n" +
                            "BEGIN\n" +
                            "      SELECT image, image_sig INTO imageObj, image_sigObj \n" +
                            "         FROM image WHERE imageId=999 FOR UPDATE;\n" +
                            "      -- generate a signature\n" +
                            "      image_sigObj.generateSignature(imageObj);\n" +
                            "      UPDATE image SET image_sig = image_sigObj WHERE imageId =999;\n" +
                            "    COMMIT;\n" +
                            "END;";
                        conn.execute(updateORDIMAGESQL, [], {autoCommit: true}, function (err, result) {
                            if (err) {
                                console.error(err.message);
                            } else {
                                console.log("generated image signature")
                            }
                        });

                        sftp.connect({
                            host: 'kain.imn.htwk-leipzig.de',
                            // port: '8080',
                            username: CONFIG.username,
                            password: CONFIG.password,
                            // port: 22,
                        }).then(() => {
                            console.log(req.file.originalname);
                            sftp.get(req.file.filename).then((stream) => {
                                conn.execute(
                                    "UPDATE image SET imageblob = :b WHERE imageid = 999",
                                    {b: stream},
                                    function (err, result) {
                                        if (err) {
                                            console.log(err)
                                        }
                                    });
                                console.log('inserted image as blob');
                                sftp.delete(req.file.originalname);
                                console.log('deleted image from kain.htwk')
                            });
                        }).catch((err) => {
                            console.log(err, 'catch error');
                        });

                        const selectSQL = `
                        select b.imageblob, b.name,  b.usk, b.platformname, b.price, publisher.pubname, b.imageid
                        from
                            (select a.imageblob, a.name,  a.usk, a.platformname, a.price, a.imageid, a.gameid, publishedby.pubid
                            from
                                (select platforminfo.imageblob, platforminfo.name,  platforminfo.usk, platform.platformname, platforminfo.price, platforminfo.imageid, platforminfo.gameid
                                from
                                    (select platformids.imageid, platformids.imageblob, platformids.gameid, platformids.name, platformids.usk, platformids.platformid, releasedon.price
                                    from
                                        (select gameinfos.imageid, gameinfos.imageblob, gameinfos.gameid, gameinfos.name, gameinfos.usk, belongstoimage.platformid
                                        from
                                            (select gameids.imageid, gameids.imageblob, gameids.gameid, game.name, game.usk
                                            from
                                                (select imageids.imageid, imageids.imageblob, gamehasimage.gameid
                                                from
                                                    (SELECT Q.imageid, Q.imageblob
                                                    FROM image Q, image E
                                                    WHERE E.imageid=999 AND Q.imageid != E.imageid
                                                    AND ORDSYS.IMGSimilar(Q.image_sig, E.image_sig,
                                                   'color="0.4" texture="0.3" shape="0.3" location="0.0"', 25) = 1) imageids
                                                inner join gamehasimage on imageids.imageid = gamehasimage.imageid) gameids
                                            inner join game on gameids.gameid = game.gameid) gameinfos
                                        inner join belongstoimage on gameinfos.imageid = belongstoimage.imageid) platformids
                                    inner join releasedon on platformids.gameid = releasedon.gameid AND platformids.platformid = releasedon.platformid) platforminfo
                                inner join platform on platforminfo.platformid = platform.platformid) a
                            inner join publishedby on a.gameid = publishedby.gameid) b
                        inner join publisher on b.pubid = publisher.pubid
                        `;

                        const selectSQLResult = await conn.execute(selectSQL, []);
                        res.send(selectSQLResult);
                        console.log(selectSQLResult.rows);
                    });
                }
            );

        } catch
            (err) {
            console.log('Ouch!', err);
        } finally {
            if (conn) { // conn assignment worked, need to close
                conn.close();
            }
        }
    }
);

module.exports = app;
