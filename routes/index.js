let oracledb = require('oracledb');
var express = require('express');
const cors = require('cors');
const app = express.Router();

const config = {
    user: 'mmdb09',
    password: 'mmdb09',
    connectString: 'ora10glv.imn.htwk-leipzig.de:1521/ora10glv'
};

app.use(cors());

app.all('/', function(req, res, next) {
    console.log({method: req.method, url: req.url});
    next();
});

module.exports = app;
