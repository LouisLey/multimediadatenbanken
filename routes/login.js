let oracledb = require('oracledb');
const app = require('./index');

const config = {
    user: 'mmdb09',
    password: 'mmdb09',
    connectString: 'ora10glv.imn.htwk-leipzig.de:1521/ora10glv'
};

/* Login user */
app.post('/login', async function (req, res) {
    let conn;
    const user = {
        name: req.body.name,
        password: req.body.password
    };

    try {
        conn = await oracledb.getConnection(config);

        const userExists = await checkIfUserExists(user, conn);
        if (userExists) {
            const passwordCorrect = await checkIfPasswordCorrect(user, conn);
            passwordCorrect ? res.json({login: true}) : res.json({login: false, error: 'passwordIncorrect'});
        } else {
            res.json({login: false, error: 'userNotExisting'});
        }
    } catch (err) {
        console.log('Ouch!', err);
    } finally {
        if (conn) { // conn assignment worked, need to close
            await conn.close()
        }
    }
});

/* Register user */
app.post('/register', async function (req, res) {
    let conn;
    const user = {
        name: req.body.name,
        password: req.body.password
    };

    try {
        conn = await oracledb.getConnection(config);

        const userExists = await checkIfUserExists(user, conn);
        if (userExists) {
            res.json({registered: false});
        } else {
            await insertUser(user, conn);
            res.json({registered: true});
        }
    } catch (err) {
        console.log('Ouch!', err);
    } finally {
        if (conn) { // conn assignment worked, need to close
            await conn.close();
        }
    }
});

const checkIfPasswordCorrect = async function (user, conn) {
    const sqlQuery = `SELECT * FROM myuser WHERE name='${user.name}' AND password='${user.password}'`;
    const result = await conn.execute(sqlQuery);
    console.log(result);
    return (result.rows.length > 0);
}

const checkIfUserExists = async function (user, conn) {
    const sqlQuery = `SELECT * FROM myuser WHERE name='${user.name}'`;
    const result = await conn.execute(sqlQuery);
    return (result.rows.length > 0);
};

const insertUser = async function (user, conn) {
    const sqlQuery = `INSERT INTO myuser (name, password) VALUES ('${user.name}', '${user.password}')`;
    await conn.execute(sqlQuery, [], {autoCommit: true});

    const setWishlistId = `INSERT INTO wishlist (wishlistid) VALUES ('${user.name}')`;
    await conn.execute(setWishlistId, [], {autoCommit: true});
}

module.exports = app;
